#ifndef _DEBUG_HELPERS_H_
#define _DEBUG_HELPERS_H_

#include "debug_helpers.c"

void usb_show_interface_descriptor(struct usb_interface_descriptor * desc);
void usb_show_endpoint_descriptor(struct usb_endpoint_descriptor * desc);
void usb_show_device_descriptor(struct usb_device_descriptor *desc);
void usb_show_config_descriptor(struct usb_config_descriptor * desc);

#endif
