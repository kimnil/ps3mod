#ifndef __PS3MOD_H__
#define __PS3MOD_H__

// By defining __KERNEL__ and MODULE - that allows us to access kernel-level code not usually 
// available to userspace programs. Not sure how this works.
//
// Skeleton of this file comes from here:
// http://blog.markloiseau.com/2012/04/hello-world-loadable-kernel-module-tutorial/
#undef __KERNEL__
#define __KERNEL__

#undef MODULE
#define MODULE

// Linux Kernel/LKM headers
#include <linux/module.h> // included for all kernel modules
#include <linux/init.h> // we need this for __init and __exit macros
#include <linux/kernel.h> // included for KERN_INFO
#include <linux/fs.h> // needed for  file_operations struct
#include <linux/usb.h>
#include <linux/mutex.h> // Needed to prevent IO during disconnects

#define DEVICE_IDENTIFIER "ps3mod"
#define DEVICE_NAME "PS3 Controller (ps3mod)"

// License and authors
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kim Nilsson, Otto Nordgren, Linus Eriksson");
MODULE_DESCRIPTION("This module implements a driver to a PS3 six axis controller");

// Device specific data
#define VENDOR_ID   0x054c
#define PRODUCT_ID  0x0268

// Minor number base
#define MINOR_NUMBER_BASE 23

extern struct usb_driver usb_device_driver;
extern struct usb_class_driver ps3mod_class;

// Structure to hold all our device specific stuff
struct ps3controller_device {
  struct input_dev *      input_dev;      // the input device
  struct usb_device *     udev;           // the usb device for this device
  struct usb_interface *  interface;      // the intercace for this devise
  struct urb *            int_in_urb;        // in interrupt urb
  unsigned char *         int_in_buffer; // the buffer to receive data to
  size_t                  int_in_size;   // in buffer size
  __u8                    int_in_endpointAddr;
  __u8                    int_out_endpointAddr; // addresses to endpoints
  __u8                    int_in_interval; // interrupt frequency
  __u8                    int_out_interval; // interrupt frequency
  struct mutex            io_mutex;

  struct kref             kref; // not sure what this is!

  char phys[64];         // physical device path

};

#endif // __PS3MOD_H__
