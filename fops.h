#ifndef FOPS_H__
#define FOPS_H__

#include <linux/fs.h>
#include <linux/usb.h>
#include <linux/slab.h>
#include <linux/hid.h>
#include <linux/input.h>
#include <linux/usb/input.h>

extern int device_open(struct inode *inode, struct file *file);
extern int device_release(struct inode *inode, struct file *file);

extern ssize_t device_read(struct file *filp,
                           char *buffer,
                           size_t length,
                           loff_t *offset);

extern int device_probe(struct usb_interface *interface,
                        const struct usb_device_id *id);

extern void device_disconnect(struct usb_interface *interface);

void int_in_complete_callback(struct urb *urb, struct pt_regs *regs);

void hid_fixup_sony_ps3_controller(struct usb_device *dev, int ifnum);

int input_open(struct input_dev *dev);
void input_close(struct input_dev *dev);

#endif
