obj-m := ps.o
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

ps-objs := ps3mod.o fops.o

all:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
 
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean

