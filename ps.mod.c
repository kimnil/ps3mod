#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x15b2dc7b, "module_layout" },
	{ 0x68c70c2d, "kmalloc_caches" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0xc05ca4fa, "dev_set_drvdata" },
	{ 0x71c60043, "usb_kill_urb" },
	{ 0x72fa16a5, "usb_deregister_dev" },
	{ 0x460a6565, "mutex_unlock" },
	{ 0xc5e0de59, "input_set_abs_params" },
	{ 0xa8bb5c8b, "input_event" },
	{ 0xdd0a2ba2, "strlcat" },
	{ 0xc7c4c313, "usb_deregister" },
	{ 0x3c743db9, "__mutex_init" },
	{ 0x50eedeb8, "printk" },
	{ 0xb4390f9a, "mcount" },
	{ 0x85fb75f4, "usb_register_dev" },
	{ 0xddbf4a75, "usb_control_msg" },
	{ 0x7dd48cec, "mutex_lock" },
	{ 0x4059792f, "print_hex_dump" },
	{ 0x4c39c9f6, "usb_submit_urb" },
	{ 0x784ad94, "usb_get_dev" },
	{ 0xf634e88d, "input_register_device" },
	{ 0x681822ec, "usb_put_dev" },
	{ 0xf434fdeb, "usb_find_interface" },
	{ 0x2c9189e, "kmem_cache_alloc_trace" },
	{ 0x37a0cba, "kfree" },
	{ 0x60dacd3, "input_unregister_device" },
	{ 0x24dfb53f, "usb_register_driver" },
	{ 0xb81960ca, "snprintf" },
	{ 0xc302e718, "dev_get_drvdata" },
	{ 0x5085c705, "usb_alloc_urb" },
	{ 0xfda321fa, "input_allocate_device" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("usb:v054Cp0268d*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "8358C14769BEEA768E53AD8");
