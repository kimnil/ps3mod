#include "ps3mod.h"
#include "fops.h"
#include <linux/hid.h>

// Create a list of the devices this driver supports and register it to
// the usb subsytem. This allows the hotplug system to automatically load driver.
// Note: NULL termiated list
static const struct usb_device_id supported_devices_table[] = {
  { USB_DEVICE(VENDOR_ID, PRODUCT_ID) },
  //{ USB_DEVICE(0x054c, 0x0268) }, // hard coded
  //{ .driver_info = 42 }, // match all devices 
  { } // terminating entry
};
MODULE_DEVICE_TABLE(usb, supported_devices_table);

//MODULE_DEVICE_TABLE(usb, supported_devices_table);

// We need this struct to register the driver to the usb core
struct usb_driver usb_device_driver = {
  .name = DEVICE_IDENTIFIER,
  .probe = device_probe,
  .disconnect = device_disconnect,
  .id_table = supported_devices_table
  //.fops // left empty for now, not sure if needed
  //.minor// left empty for now, not sure if needed
};

// File operations
struct file_operations ps3mod_fops = {
    .owner = THIS_MODULE,
    .read = device_read,
    .write = NULL,
    .open = device_open,
    .release = device_release,
    .flush = NULL,
};

// Some usb class thing
struct usb_class_driver ps3mod_class = {
    .name = "ps3mod%d",
    .fops = &ps3mod_fops,
    .minor_base = MINOR_NUMBER_BASE,
};

// Entry function
static int __init ps3mod_init(void) 
{
  int result;

  printk(KERN_INFO "(ps3mod) initializing ps3mod!\n");

  // register driver with usb subsystem
  result = usb_register(&usb_device_driver);

  if (result) {
    printk(KERN_INFO "something went wrong registering the drive! error code: %d\n", result);
    return -1;
  } else {
    printk(KERN_INFO "successfully registered driver to usb subsystem\n");
  }


  return result; // 0 for success
}

// Exit function
static int __exit ps3mod_cleanup(void) 
{
  printk(KERN_INFO "(ps3mod) cleaning up ps3mod!\n");

  // remove the driver from the usb system
  usb_deregister(&usb_device_driver);

  return 0; // 0 for success
}


// Register the init and cleanup functions to the kernel
module_init(ps3mod_init);
module_exit(ps3mod_cleanup);
