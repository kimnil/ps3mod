#include "fops.h"
#include "ps3mod.h"
#include "debug_helpers.h"

/* 
 * Called when the /dev/device file is opened
 * like so: cat /dev/ps3mod
 *
*/
int device_open(struct inode *inode, struct file *file) {
  struct ps3controller_device *dev;
  struct usb_interface *interface;
  
  int retval = 0;
  int subminor;

  printk(KERN_DEBUG " hello from device_open!\n");

  // Get the minor number of the device
  subminor = iminor(inode);

  // Fetch ref to the interface
  interface = usb_find_interface(&usb_device_driver, subminor);
  if (!interface) {
    printk(KERN_ALERT " Oh crap! the device seems to be missing! (interface was null)\n");
    retval = -ENODEV;
    goto exit;
  }

  // Fetch ps3controller_device 
  dev = usb_get_intfdata(interface);
  if (!dev) {
    printk(KERN_ALERT " Oh crap! the device seems to be missing! (dev was null)\n");
    retval = -ENODEV;
    goto exit;
  } else {
    printk(KERN_DEBUG " Found both device and interface. dev int_in_endpointAddr is %d\n", dev->int_in_endpointAddr);
  }

// Label for exiting
// NOTE: Make sure appropriate retval is set before jumping here.
exit:
  return retval;
}

/*
 * This is the callback method used in reading operations
 *
 */
void int_in_complete_callback(struct urb *urb, struct pt_regs *regs) {
  int retval = -1;
  unsigned char val;

  struct ps3controller_device *dev = urb->context;
  struct input_dev *input_dev = dev->input_dev;

  printk(KERN_DEBUG " hello from int_in_complete_callback!\n");
  printk(KERN_DEBUG " urb->status = %d\n", urb->status);
  print_hex_dump(KERN_DEBUG, "raw data: ", DUMP_PREFIX_ADDRESS, 32, 1, urb->transfer_buffer, urb->transfer_buffer_length, 1);

  // Resubmit the URB (has to be done manually since kernel ~2.6)
  retval = usb_submit_urb(urb, GFP_KERNEL);
  if (retval != 0) {
    printk(KERN_ALERT " Oh crap! Could not resubmit urb, error code %d\n", retval);
  } else {
    printk(KERN_DEBUG " Successfully submitted urb\n");
  }

  // buttons
  val = ((char *)(urb->transfer_buffer))[3] & 0x10;
  printk(KERN_DEBUG "Triangle (BTN_A): %d\n", val);
  input_report_key(input_dev, BTN_A, ((val > 0) ? 1 : 0));

  val = ((char *)(urb->transfer_buffer))[3] & 0x20;
  printk(KERN_DEBUG "Circle (BTN_B): %d\n", val);
  input_report_key(input_dev, BTN_B, ((val > 0) ? 1 : 0));

  val = ((char *)(urb->transfer_buffer))[3] & 0x40;
  printk(KERN_DEBUG "Cross (BTN_X): %d\n", val);
  input_report_key(input_dev, BTN_X, ((val > 0) ? 1 : 0));

  val = ((char *)(urb->transfer_buffer))[3] & 0x80;
  printk(KERN_DEBUG "Square (BTN_Y): %d\n", val);
  input_report_key(input_dev, BTN_Y, ((val > 0) ? 1 : 0));
  
  input_sync(input_dev);

  // d-pad
  val = ((char *)(urb->transfer_buffer))[2] & 0x80;
  printk(KERN_DEBUG "LEFT (BTN_LEFT): %d\n", val);
  input_report_key(input_dev, BTN_LEFT, ((val) ? 1 : 0));

  val = ((char *)(urb->transfer_buffer))[2] & 0x20;
  printk(KERN_DEBUG "RIGHT (BTN_RIGHT): %d\n", val);
  input_report_key(input_dev, BTN_RIGHT, ((val > 0) ? 1 : 0));

  // right joystick
  /*
  val = ((char *)(urb->transfer_buffer))[8];
  printk(KERN_DEBUG "RIGHT JOYSTICK (ABS_RX): %d\n", val);
  input_report_abs(input_dev, ABS_RX, val);
  val = ((char *)(urb->transfer_buffer))[9];
  printk(KERN_DEBUG "RIGHT JOYSTICK (ABS_RY): %d\n", val);
  input_report_abs(input_dev, ABS_RY, val);
  */

  // left joystick
  val = ((char *)(urb->transfer_buffer))[6];
  printk(KERN_DEBUG "LEFT JOYSTICK (ABS_X): %d\n", val);
  input_report_abs(input_dev, ABS_X, val);
  val = ((char *)(urb->transfer_buffer))[7];
  printk(KERN_DEBUG "LEFT JOYSTICK (ABS_Y): %d\n", val);
  input_report_abs(input_dev, ABS_Y, val);

  return;
} 


/* 
 * Called when a processes closes file.
 *
*/
int device_release(struct inode *inode, struct file *file) {
  printk(KERN_DEBUG " hello from device_release!\n");
  return 0;
}

/*
 * Called when a process reads from a already opened file
*/
ssize_t device_read(struct file *filp, // see include/linux/fs.h
                           char *buffer,      // buffer to fill with data
                           size_t length,     // length of buffer
                           loff_t *offset) {
  printk(KERN_DEBUG " hello from device_read!\n");
  return 0;
}

/*
 * When a device is plugged into the system that matches our device table probe is called. 
*/
int device_probe(struct usb_interface *interface,
                        const struct usb_device_id *id) {
  struct ps3controller_device *dev;
  struct usb_host_interface *iface_desc;
  struct usb_endpoint_descriptor *endpoint;
  struct input_dev *input_dev;
  struct urb *int_urb;

  int retval = -ENOMEM;
  int i;

  printk(KERN_DEBUG "(ps3mod) device_probe!\n");

  // allocate memory for our ps3controller device struct and the input device
  dev = kmalloc(sizeof(struct ps3controller_device), GFP_KERNEL); 
  input_dev = input_allocate_device();
  if (!dev || !input_dev) {
      printk(KERN_ALERT " out of memory!\n");
      retval = -ENOMEM;
      goto error;
  }
  memset(dev, 0x00, sizeof (*dev)); // initialize to 0
  kref_init(&dev->kref);

  // Initialize the io mutex
  mutex_init(&dev->io_mutex);

  // get the usb device - i also think that this increases some usage counter
  dev->udev = usb_get_dev(interface_to_usbdev(interface)); 
  dev->interface = interface;

  // Print some USB device debug information
  usb_show_device_descriptor(&(dev->udev)->descriptor);

  // fetch curent interface descriptor
  iface_desc = interface->cur_altsetting;

  // Print some debug info about the interface
  usb_show_interface_descriptor(&iface_desc->desc);

  // Set up the input device
  dev->input_dev = input_dev; 

  usb_make_path(dev->udev, dev->phys, sizeof(dev->phys));
  strlcat(dev->phys, "/input0", sizeof(dev->phys));

  input_dev->name = DEVICE_NAME;
  input_dev->phys = dev->phys;
  usb_to_input_id(dev->udev, &input_dev->id);
  input_dev->dev.parent = &interface->dev;

  input_set_drvdata(input_dev, dev);

  // Open and close-callbacks
  input_dev->open = input_open;
  input_dev->close = input_close;

  // What kind of events do we want to trigger?
  input_dev->evbit[0] = BIT_MASK(EV_KEY) | BIT_MASK(EV_ABS);

  /* set up standard buttons */
  set_bit(BTN_A, input_dev->keybit);
  set_bit(BTN_B, input_dev->keybit);
  set_bit(BTN_X, input_dev->keybit);
  set_bit(BTN_Y, input_dev->keybit);
  set_bit(BTN_LEFT, input_dev->keybit);
  set_bit(BTN_RIGHT, input_dev->keybit);

  /* left n' right joystick */
  set_bit(ABS_X, input_dev->absbit);
  set_bit(ABS_Y, input_dev->absbit); 
  //set_bit(ABS_RX, input_dev->absbit);
  //set_bit(ABS_RY, input_dev->absbit);
  input_set_abs_params(input_dev, ABS_X, 0, 255, 4, 0);
  input_set_abs_params(input_dev, ABS_Y, 0, 255, 4, 0);
  //input_set_abs_params(input_dev, ABS_RX, 0, 255, 4, 0);
  //input_set_abs_params(input_dev, ABS_RY, 0, 255, 4, 0);

  /*
  __set_bit(BTN_UP, input_dev->keybit);
  __set_bit(BTN_DOWN, input_dev->keybit);
  */

  retval = input_register_device(input_dev);
  if(retval) {
    printk(KERN_DEBUG "error! could not register input device!\n");
    goto error;
  } else {
    printk(KERN_DEBUG "successfully registered input device\n");
  }
    
  // Iterate over all the endpoints in this given interface
  for(i = 0; i < iface_desc->desc.bNumEndpoints; i++) {
      endpoint = &iface_desc->endpoint[i].desc;

      // print some debug information about endpoint
      usb_show_endpoint_descriptor(endpoint);

      // Do we have an OUT INT endpoint?
      if (usb_endpoint_is_int_out(endpoint)) {
        // We might want to handle these later, for example, what about sending vibrations?
      }

      // Do we have an IN INT endpoint?
      if (usb_endpoint_is_int_in(endpoint)) {
          printk(KERN_DEBUG "found a USB_DIR_IN, INT\n");

          // Save some information about the endpoint (URB needs this later)
          dev->int_in_size = endpoint->wMaxPacketSize;
          dev->int_in_endpointAddr = endpoint->bEndpointAddress;
          dev->int_in_interval = endpoint->bInterval;

          // Create data buffer
          dev->int_in_buffer = kmalloc(dev->int_in_size, GFP_KERNEL);
          if(!dev->int_in_buffer) {
              retval = -ENOMEM;
              goto error;
          }
  
          // Send a GET_REPORT control message - otherwise the controller is "dead"
          hid_fixup_sony_ps3_controller(dev->udev, iface_desc->desc.bInterfaceNumber); 
      }
  }

  // Just make sure that we found an IN INT endpoint.
  if (!(dev->int_in_endpointAddr)) {
      retval = -1;
      printk(KERN_DEBUG "error! could not find IN INT endpoint\n");
      goto error;
  }

  // Store our ps3mod struct (dev) into the usb interface struct.
  usb_set_intfdata(interface, dev);

  // Register usb device
  retval = usb_register_dev(interface, &ps3mod_class);
  if (retval) {
      printk(KERN_ALERT "could not register usb devise. got this error code %d\n", retval);
      usb_set_intfdata(interface, NULL);
      goto error;
  }

  // Allocate memory for the urb
  int_urb = usb_alloc_urb(0, GFP_KERNEL);
  if (!int_urb) {
    printk(KERN_DEBUG " error, could not initialize urb\n");
    retval = -ENOMEM;
    goto error;
  }

  // Fill the urb
  usb_fill_int_urb(int_urb,                                              // The urb to fill
                   dev->udev,                                            // The device we are talking to
                   usb_rcvintpipe(dev->udev, dev->int_in_endpointAddr),  // The endpoint of that device 
                   dev->int_in_buffer,                                   // Buffer and its length
                   dev->int_in_size,
                   int_in_complete_callback,                             // Callback handler
                   dev,                                                  // Callback blob
                   dev->int_in_interval);                                // Interval

  // Store the URB in the ps3controller_device for later use.
  dev->int_in_urb = int_urb; 

  printk(KERN_ALERT " ps3mod are now attached to minor number %d\n", interface->minor);
  return 0;

error:
/*
  if(dev)
    kref_put(&dev->kref, NULL); // NULL causes crash here.
*/
    return retval;
}

/*
 * This is called when a supported device is disconnected
*/
void device_disconnect(struct usb_interface *interface) {
    struct ps3controller_device *dev;

    printk(KERN_DEBUG " hello from device_disconnected!\n");

    dev = usb_get_intfdata(interface);
    usb_set_intfdata(interface, NULL);

    // unregister input device
    input_unregister_device(dev->input_dev);

    // release minor number
    usb_deregister_dev(interface, &ps3mod_class);

    // Set the interface to NULL to prevent more IO
    mutex_lock(&dev->io_mutex);
    dev->interface = NULL;
    mutex_unlock(&dev->io_mutex);

    // Kill the URB
    usb_kill_urb(dev->int_in_urb);

    // release the use of the usb device structure, not sure if this is necesary
    usb_put_dev(dev->udev);

    // Release the memory for the ps3controller struct and its internal buffer
    kfree(dev->int_in_buffer);
    kfree(dev);

    return;
}

/*
 * Sending HID_REQ_GET_REPORT changes the operation mode of the ps3 controller
 * to "operational". Without this, the ps3 controller will not report any
 * events.
 */
void hid_fixup_sony_ps3_controller(struct usb_device *dev, int ifnum)
{
  int result;
  char *buf = kmalloc(18, GFP_KERNEL);

  if (!buf)
    return;

  result = usb_control_msg(dev, usb_rcvctrlpipe(dev, 0),
  HID_REQ_GET_REPORT,
  USB_DIR_IN | USB_TYPE_CLASS |
  USB_RECIP_INTERFACE,
  (3 << 8) | 0xf2, ifnum, buf, 17,
  USB_CTRL_GET_TIMEOUT);

  if (result < 0)
    printk(KERN_DEBUG "Failed to send HID_REQ_GET_REPORT to controller, error code %d \n", result);

  kfree(buf);
}

/* 
 * Called when the driver is first calling input_open_device
*/
int input_open(struct input_dev *input_dev) {
  struct ps3controller_device *dev;
  int retval = -1;

  dev = input_get_drvdata(input_dev);

  printk(KERN_DEBUG " hello from input_open!\n");
  // Submit the URB to the usb core
  retval = usb_submit_urb(dev->int_in_urb, GFP_KERNEL);
  if (retval != 0) {
    printk(KERN_ALERT " Oh crap! Could not submit urb, error code %d\n", retval);
    return retval;
  } else {
    printk(KERN_DEBUG " Successfully submitted urb\n");
  }

  return 0;
}

/* 
 * Called when the last user calls input_close_device
*/
void input_close(struct input_dev *input_dev) {
  struct ps3controller_device *dev;

  dev = input_get_drvdata(input_dev);

  printk(KERN_DEBUG " hello from input_close!\n");
  usb_kill_urb(dev->int_in_urb);
}
